const express = require('express')
const bodyParser = require('body-parser')


const routerUser = require('./routes/login')

const app = express()


app.use(bodyParser.json())

app.get('/', (request, response) => {
  response.send('<h1>Welcome to my backend</h1>')
})


app.use('/user', routerUser)

app.listen(3537, '0.0.0.0', () => {
  console.log('server started on port 3537')
})
