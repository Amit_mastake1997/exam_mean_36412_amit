 create database Mean_exam_db;
 use Mean_exam_db;

 create table User_Tbl(id integer primary key auto_increment,email varchar(20) NOT NULL,password varchar(20) NOT NULL);

create table Category_Tbl(category_id integer primary key auto_increment,category_name varchar(20) NOT NULL);
 create table Book_Tbl(book_id integer primary key auto_increment,name varchar(50) NOT NULL,author varchar(50) NOT NULL,image varchar(100) NULL,price float NOT NULL,category_id integer FOREIGN KEY REFERENCE(category_id),available_count integer NULL);

 insert into User_Tbl values(1,'test12@gmail.com','tset');
 insert into User_Tbl values(2,'test1@gmail.com','test');

 insert into Category_Tbl values(001,'Electronics');
insert into Category_Tbl values(002,'Home Decore');

insert into Book_Tbl values(1,'DS','R.C.Sharma','',1000,001,10);
insert into Book_Tbl values(2,'MS','R.C.Sharma','',2000,002,5);